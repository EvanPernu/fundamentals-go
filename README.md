Golang implementations of the various data structures, algorithms, etc. found in "Cracking the Coding Interview".

Note: For educational purposes, I've implemented the data structures as though Golang's slices and StringBuilder don't exist (in favor of my own ArrayList and StringBuilder).