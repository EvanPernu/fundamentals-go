// Golang implementations and information for common sorting algorithms.
//
// Space complexities only concern auxilliary memory.

package sortingalgorithms

/*
 * Sorting algorithms
 */

// Insertion Sort
// For each element in the array, shift it left until it's in the correct place.
//
// Best:   O(n)
// Avg.:   O(n^2)
// Worst:  O(n^2)
// Space:  O(1)
// Stable: yes
// Pros:   Easy to implement
// Cons:   Inefficient
func InsertionSort(arr []int) {
	for i := 1; i < len(arr); i++ {
		for j := i; j >= 1; j-- {
			if arr[j] < arr[j-1] {
				swap(arr, j, j-1)
			}
		}
	}
}

// Shell Sort
// An optimized insertion sort that first partially sorts the array by choosing a gap n,
// making sure every nth element is sorted with respect to each other (via insertion sort),
// and then decreasing n and repeating until n = 1 (in which case it becomes a normal insertion sort).
//
// Shell Sort is named after some nerd named Donald Shell who invented it in 1959.
//
// Best:   O(nlog(n))
// Avg.:   O(n(log(n))^2)
// Worst:  O(n^2)
// Space:  O(1)
// Stable: yes
// Pros:   More efficient than insertion sort
// Cons:   Less efficient than quicksort or mergesort
func ShellSort(arr []int) {
	gap := max(1, len(arr)/3%1)
	if gap == 1 {
		return
	}

	for {
		for i := 0; i < len(arr); i += gap {
			if i > len(arr)-1 {
				i = len(arr) - 1
			}
			for j := i; j <= gap; j -= gap {
				if j < 0 {
					j = 0
				}
				if arr[j] > arr[max(0, j-gap)] {
					swap(arr, j, max(0, j-gap))
				}
			}
		}

		gap = max(1, gap/3%1)
		if gap == 1 {
			break
		}
	}
}

/*
 * Helper functions
 */

func swap(arr []int, i, j int) {
	tmp := arr[j]
	arr[j] = arr[i]
	arr[i] = tmp
}

func max(i, j int) int {
	if i < j {
		return j
	}
	return i
}
