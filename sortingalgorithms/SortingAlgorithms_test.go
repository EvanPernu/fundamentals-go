package sortingalgorithms

import (
	"reflect"
	"testing"
)

func TestInsertionSort(t *testing.T) {
	for _, arg := range Args {
		InsertionSort(arg.arg)
		if !reflect.DeepEqual(arg.arg, arg.exp) {
			t.Fatalf("exp: %v, got: %v", arg.exp, arg.arg)
		}
	}

}

func TestShellSort(t *testing.T) {
	for _, arg := range Args {
		ShellSort(arg.arg)
		if !reflect.DeepEqual(arg.arg, arg.exp) {
			t.Fatalf("exp: %v, got: %v", arg.exp, arg.arg)
		}
	}

}

/*
 * Helper data
 */

var Args = []struct {
	arg []int
	exp []int
}{
	{
		arg: []int{},
		exp: []int{},
	},
	{
		arg: []int{1},
		exp: []int{1},
	},
	{
		arg: []int{1, 2},
		exp: []int{1, 2},
	},
	{
		arg: []int{2, 1},
		exp: []int{1, 2},
	},
	{
		arg: []int{1, 2, 3},
		exp: []int{1, 2, 3},
	},
	{
		arg: []int{3, 2, 1},
		exp: []int{1, 2, 3},
	},
	{
		arg: []int{2, 1, 3},
		exp: []int{1, 2, 3},
	},
	{
		arg: []int{3, 1, 2},
		exp: []int{1, 2, 3},
	},
	{
		arg: []int{8, 9, 5, 3, 4, 2, 1, 6, 7, 4, 4, 9},
		exp: []int{1, 2, 3, 4, 4, 4, 5, 6, 7, 8, 9, 9},
	},
}
