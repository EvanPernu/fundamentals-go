package hashtable

import (
	"fmt"
	"testing"
)

func TestFunctionality(t *testing.T){
	table := New(5)

	// Insert() and the Get() 20 things 
	for i := 1; i <= 20; i++ {
		key := fmt.Sprintf("key_%d", i)
		val := fmt.Sprintf("val_%d", i)
		table.Insert(key, val)

		ok, got := table.Get(key)
		if !ok || got != val {
			t.Fatalf("Insert(%s) --> Get(%s) - expected: %t, %s, got: %t, %v\ntable: %+v", 
					key, key, true, val, ok, got, table)
		}
	}

	// Delete() and then ensure Get() fails for those 20 things
	for i := 1; i <= 20; i++ {
		key := fmt.Sprintf("key_%d", i)
		
		table.Delete(key)

		ok, got := table.Get(key)
		if ok || got != nil {
			t.Fatalf("Delete(%s) --> Get(%s) - expected: %t, %v, got: %t, %v\nTtable: %+v", 
					key, key, false, nil, ok, got, table)
		}
	}
}

func TestToString(t *testing.T){
	table := New(1)
	table.Insert("key_1", "val_1")
	table.Insert("key_2", "val_2")
	if table.String() != "" {
		fmt.Errorf("String() expected: \"%s\", got: \"%s\"")
	}
}