// Hash table that stores string keys and interface{} values with a 1:1 mapping.
//
// This implementation uses the FNV hashing algorithm, which is performant and does a good job of mitigating
// collisions but should not be used for any security applications e.g. storing passwords. In that case
// you'd want to use a salted hash with a security-oriented hashing algorithm.
package hashtable

import (
	"fmt"
	"hash/fnv"
)

type HashTable struct {
	lists []hashLinkedList
}

func New(size int) *HashTable {
	return &HashTable{
		lists: make([]hashLinkedList, size),
	}
}

// Returns false and does nothing if the key is already in use.
func (table *HashTable) Insert(key string, value interface{}) bool {
	i := fnvHash(key) % len(table.lists)
	return table.lists[i].append(key, value)
}

// Deletes the entry with the given key from the table, if it exists.
func (table *HashTable) Delete(key string) {	
	for i := 0; i < len(table.lists); i++ {
		ok := table.lists[i].delete(key)
		if ok {
			return
		}
	}
}


// Returns a bool denoting whether or not the key was found, and the value of the matching key (or nil, if not found)
func (table *HashTable) Get(key string) (bool, interface{}) {
	for i := 0; i < len(table.lists); i++ {
		ok, value := table.lists[i].get(key)
		if ok {
			return true, value
		}
	}
	return false, nil
}

// e.g. "[(foo, bar), (baz, qux)], [(meaning, 42), (rowdy, 3)]"
func (table *HashTable) String() string {
	if len(table.lists) == 0 {
		return ""
	}

	res := table.lists[0].String()

	for i := 1; i < len(table.lists); i++ {
		res = fmt.Sprintf("%s, %s", res, table.lists[i].String())
	}
	return res
}

// FNV hashing algorithm
func fnvHash(key string) int {
	h := fnv.New32a()
        h.Write([]byte(key))
        return int(h.Sum32()) // TODO is there an alternative to sum32 where we don't have to cast?
}

/*
 * Linked list used internally
 */

// Linked list designed for use in a hash table 
type hashLinkedList struct {
	first *node
}

type node struct {
	key string
	value interface{}
	next *node
	prev *node	
}

// Returns a bool denoting whether or not the key was found, and the value of the matching key (or nil, if not found)
func (list *hashLinkedList) get(key string) (bool, interface{}) {
	res := list.lookup(key)
	if res != nil {
		return true, res.value
	}else {
		return false, nil
	}
}

// Deletes the node with the given key from the list, if it exists.
// Returns true if the node was found and deleted.
func (list *hashLinkedList) delete(key string) bool {
	res := list.lookup(key)
	if res != nil {
		if res == list.first {
			list.first = res.next
			res.next = nil
		} else if res.next != nil {
			if res.prev != nil {
				res.prev.next = res.next
			} else {
				list.first = res.next
			}	
			res.next = nil
			res.prev = nil
		}
		return true
	} else {
		return false
	}
}

// Returns a reference to the node with the given key, or nil if it doesn't exist.
func (list *hashLinkedList) lookup(key string) *node {
	cur := list.first
	if cur == nil {
		return nil
	}

	for {
		if cur.key == key {
			return cur
		} else if cur.next != nil {
			cur = cur.next
		} else {
			break
		}
	}
	return nil
} 

// Returns false  and does nothing if the key is already in use.
func (list *hashLinkedList) append(key string, value interface{}) bool {
	cur := list.first
	new := &node{
		key: key,
		value: value,
	}
	if cur == nil {
		list.first = new
		return true
	} 
	
	for {
		if cur.next != nil {
			if cur.key == key {
				return false
			}
			cur = cur.next
		} else {
			cur.next = new
			new.prev = cur
			return true
		}
	}
} 

// e.g. "[(foo, bar), (baz, qux), (meaning, 42)]"
func (list *hashLinkedList) String() string {
	cur := list.first
	res := ""

	if cur == nil {
		return "[]" 
	} else {
		res = fmt.Sprintf("[(%s, %v)", cur.key, cur.value)
	}

	for {
		if cur.next == nil {
			break 
		} 
		cur = cur.next
		res = fmt.Sprintf("%s, (%s, %v)", res, cur.key, cur.value)
	}
	return fmt.Sprintf("%s]", res)
}