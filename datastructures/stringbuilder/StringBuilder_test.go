package stringbuilder

import (
	"testing"
)

func TestFunctionality(t *testing.T) {
	sb := New()
	for i := 0; i < 10; i++ {
		sb.Append(i)
	}

	str := sb.String()
	expected := "0123456789"
	if str != expected {
		t.Errorf("Expected: %s, got: %v", expected, str)
	}
}
