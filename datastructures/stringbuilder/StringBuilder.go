// A StringBuilder is a performant solution to building a string that requires many appends.
// It maintains an internal ArrayList of each string to be appended, and only concatenates them
// together once when it's String() method is invoked.
//
// Appending many strings:
//     str := ""
//     for n := 0; n <10; n++ {
//      	str += strconv.Itoa(n)
//     }
//     return str
// For each iteration this solution requires a new string to be allocated in memory,
// with each character from the old string being copied over, plus one new one. This becomes
// 1 + 2 + 3 + ... + n operations, which is equal to n(n+1)/2 and is O(n^2).
//
// Conversely, with a StringBuilder:
//     sb := stringbuilder.New()
//     for n := 0; n <10; n++ {
//      	sb.Append(n)
//     }
//     return sb.String()
// The operation is completed in O(n) time.

package stringbuilder

import (
	"fmt"

	"gitlab.com/EvanPernu/fundamentals-go/datastructures/arraylist"
)

type StringBuilder struct {
	list *arraylist.ArrayList
}

func New() *StringBuilder {
	return &StringBuilder{
		list: arraylist.New(),
	}
}

// Appends the string representation of val to the internal ArrayList
func (sb *StringBuilder) Append(val interface{}) {
	sb.list.Append(fmt.Sprintf("%v", val))
}

func (sb *StringBuilder) String() string {
	res := ""
	for i := 0; i < sb.list.Length; i++ {
		str, err := sb.list.Get(i)
		if err != nil {
			panic(err)
		}
		res = fmt.Sprintf("%s%v", res, str)
	}
	return res
}
