// List with an underlying array that doubles in length each time its capacity is exceeded.

package arraylist

import (
	"fmt"
)

type ArrayList struct {
	array []interface{}
	Length int
}

func New() *ArrayList {
	return &ArrayList{
		array: make([]interface{}, 1),
		Length: 0,
	}
}

func (list *ArrayList) Append(val interface{}) {
	if len(list.array) >= list.Length {
		list.double()
	}
	list.Length += 1
	list.array[list.Length - 1] = val
}

// Removes the element at the given index, shifts all elements after that index left by one to
// fill the vacant spot.
func (list *ArrayList) Remove(index int) error {
	if index >= list.Length {
		return fmt.Errorf("Index out of bounds")
	}

	for i := index + 1; i < list.Length; i++ {
		list.array[i - 1] = list.array[i]
	}
	list.Length -= 1
	return nil
}

// Doubles the size of the internal array
func (list *ArrayList) double() {
	new := make([]interface{}, 2*len(list.array))

	for i := 0; i < len(list.array); i++ {
		new[i] = list.array[i]
	}
	list.array = new
}

func (list *ArrayList) Get(index int) (interface{}, error) {
	if index >= list.Length {
		return nil, fmt.Errorf("Index \"%d\" out of bounds", index)
	}
	return list.array[index], nil
}

func (list *ArrayList) Contains(val interface{}) bool {
	for i := 0; i < list.Length; i++ {
		if list.array[i] == val {
				return true
		}
	}
	return false
}

func (list *ArrayList) String() string {
	res := "["
	if list.Length > 0 {
		res = fmt.Sprintf("%s%v", res, list.array[0])
	}
	for i := 1; i < list.Length; i++ {
		res = fmt.Sprintf("%s, %v", res, list.array[i])
	}
	return res + "]"
}