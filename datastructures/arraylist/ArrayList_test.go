package arraylist

import (
	"testing"
)

func TestFunctionality(t *testing.T) {
	list := New()

	for i := 0; i < 10; i++ {
		list.Append(i)
		if list.array[i] != i || list.Length != i + 1{
			t.Errorf("Append(%d) - expected: list[%d] == %d and list.Length == %d, got list[%d] == %v and list.Length == %d. list: %v",
					i, i, i, i+1, i, list.array[i], list.Length, list)
		}

		contains := list.Contains(i)
		if !contains {
			t.Errorf("Append(%d) --> Contains(%d) - expected: %t, got %t. list: %v", i, i, true, contains, list)
		}
	}

	RemoveCheck := func(i int, t *testing.T) {
		oldLen := list.Length
		oldVal, err := list.Get(i)
		if err != nil {
			t.Errorf("%v", err)
		}
		err = list.Remove(i)
		if err != nil {
			t.Errorf("%v", err)
		}
		if list.Length != oldLen - 1 {
			t.Errorf("Remove(%d) --> Get(%d) - expected: list.Length=%d, got: list.Length=%d. list: %v",
					i, i, oldLen - 1, list.Length, list)
		}
		contains := list.Contains(oldVal)
		if contains {		contains := list.Contains(oldVal)
			if contains {
				t.Errorf("Remove(%d) --> Contains(%d) - expected: %t, got %t. list: %v", i, oldVal, false, contains, list)
			}
			t.Errorf("Remove(%d) --> Contains(%d) - expected: %t, got %t. list: %v", i, oldVal, false, contains, list)
		}
		newVal, err := list.Get(i)
		if err != nil {
			if list.Length != i {
				t.Errorf("%v", err)
			}
		}
		if newVal == oldVal {
			t.Errorf("Remove(%d) --> Get(%d) - expected: != %d, got: %d. list: %v", i, i, oldVal, newVal, list)
		}
	}

	RemoveCheck(0, t)
	RemoveCheck(1, t)
	RemoveCheck(list.Length - 1, t)

	err := list.Remove(list.Length)
	if err == nil {
		t.Errorf("Remove(list.Length) succeeded, expected error.")
	}
}