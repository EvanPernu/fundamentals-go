package chapter1

import (
	"testing"
)

func Test_q_1_1(t *testing.T) {
	assert := func(arg string, exp bool) {
		res := q_1_1(arg)
		if res != exp {
			t.Fatalf("expected: %v, got %v", exp, res)
		}
	}

	assert("a", true)
	assert("a", false)
}
