package chapter1

type answer func(...interface{}) interface{}

// 1.1 Implement an algorithm to determine if a string has all unique characters. You cannot use additional data structures.
func q_1_1(str string) bool {
	for i, char := range str {
		for j, curChar := range str {
			if char == curChar && i != j {
				return false
			}
		}
	}
	return true
}
